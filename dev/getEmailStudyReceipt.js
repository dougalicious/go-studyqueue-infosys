
// html template for studyReceipt
function getStudyReceiptAsHtml({ firstName, myUrl, myStudyName }) {
  myUrl = (typeof myUrl === "string") ? "" : myUrl
  return `
    <!DOCTYPE html>
    <html lang="en">

    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=4.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="">
      <title>HTML Email Template</title>
    </head>
    <style>
      body {
        font-family: Roboto, Arial, Helvetica, sans-serif;
        font-size: 15px;
        color: #000000;
        -webkit-text-size-adjust: none !important;
        -webkit-font-smoothing: antialiased !important;
        -ms-text-size-adjust: none !important;
    }

    table, tr, td {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
    }

    a:link, a:visited, a:hover, a:active {
        color: #4285f4;
        text-decoration: none;
    }

    .appleLinks a {
        color: #000000 !important;
        text-decoration: none !important;
    }

    strong {
        font-weight: bold !important;
    }

    em {
        font-style: italic !important;
    }

    .yshortcuts a span {
        color: inherit !important;
        border-bottom: none !important;
    }

    html {
        -webkit-text-size-adjust: none;
        -ms-text-size-adjust: 100%;
    }

    .ReadMsgbody1 {
        width: 100%;
    }

    .ExternalClass {
        width: 100%;
    }

    .ExternalClass * {
        line-height: 100%
    }

    td {
        -webkit-text-size-adjust: none;
    }

    a[href^=tel] {
        color: inherit;
        text-decoration: none;
    }

    .mob-hide {
        display: none !important;
    }

    div, p, a, li, td {
        -webkit-text-size-adjust: none;
    }

    td {
        text-decoration: none !important;
    }

    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }

    @media screen and (max-width:480px) {
        .pd {
            padding-left: 20px !important;
            padding-right: 20px !important;
        }
    }

    h3 {
        margin-top: 0;
        margin-bottom: 1;
    }

    #table {
        border-collapse: collapse;
    }

    #th, #td {
        padding: 10px;
        text-align: left;
        border: 1px solid #efefef;
    }

    /*                  tr:nth-child(even) {
          background-color: #eee;
        }*/

    /*                  tr:nth-child(odd) {
          background-color: #fff;
        }*/
    </style>

    <body style="margin:10 !important; padding:0px 0 0 0px !important; background-color:#FFFFFF;">
        <table width="480" style="max-width: 600px; width: 100%; margin: 0 auto; text-align: center;" cellpadding="0"
            cellspacing="0" border="0">
            <tr>
                <td valign="top" align="center" style="padding-top: 10">
                    <table border="0" align="center" cellpadding="0" cellspacing="10" width="100%">
                        <tr>
                            <td class="leftColumnContent">
                                <img src="http://services.google.com/fh/files/emails/ux_infra_logo_long.png"
                                    class="columnImage" width="230" alt="logoImageUrl"
                                    style="-ms-interpolation-mode:bicubic; height:auto; outline:none; text-decoration:none"
                                    height="auto"> 
                            </td>
                            <td
                                style="font-family:Google Sans,Arial,sans-serif;font-size:16px;font-weight:700;color:#9aa0a6;letter-spacing:-0.31px;text-align:right">
                                </td>
                        </tr>
                    </table>
                    <!-- Main card start -->
            <tr>
                <td align="center" valign="top" bgcolor="#ffffff"
                    style="-webkit-border-radius:0px; -moz-border-radius:0px; border-radius: 0px; border-left: 1px solid #efefef; border-top: 1px solid #efefef; border-right: 1px solid #efefef; border-bottom: 1px solid #efefef;">

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>         
                            <tr>
                                <td valign="top" align="left"
                                    style="font-family:Roboto, Helvetica, Arial sans-serif; font-size: 14px; line-height:24px; color: #414347; padding:30px 40px 30px 40px;">

                                    <i><small style="color: grey">FYI, managers are automatically cc’d on requests; no action is needed</i></small><br><br> Hello ${firstName},
                                    </a>
                                    <br>
                                    <p>
                                        Thank you for your study support request for <a href="${myUrl}" > ${myStudyName}</a>.
    <br> <br>
    A recruiter will be in touch within 1-2 business days with next steps.
                                    </p>
                                    
                                        
                                        Thank you,<br>
    <a href="mailto:uxr-operations@google.com" style="text-decoration=none">UX Research Operations</a>
                                    </p>
    
                            <!-- Sign off ends -->
                        </tbody>
                    </table>
                </td>
            </tr>
            <!-- Main card end -->

    </body>

    </html>
    `
}



function generateGeneralHtmlTemplate({ intro, body, signature }) {

  return `
      <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=4.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="">
        <title>HTML Email Template</title>
    </head>
    <style>
      body {
        font-family: Roboto, Arial, Helvetica, sans-serif;
        font-size: 15px;
        color: #000000;
        -webkit-text-size-adjust: none !important;
        -webkit-font-smoothing: antialiased !important;
        -ms-text-size-adjust: none !important;
    }

    table, tr, td {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
    }

    a:link, a:visited, a:hover, a:active {
        color: #4285f4;
        text-decoration: none;
    }

    .appleLinks a {
        color: #000000 !important;
        text-decoration: none !important;
    }

    strong {
        font-weight: bold !important;
    }

    em {
        font-style: italic !important;
    }

    .yshortcuts a span {
        color: inherit !important;
        border-bottom: none !important;
    }

    html {
        -webkit-text-size-adjust: none;
        -ms-text-size-adjust: 100%;
    }

    .ReadMsgbody1 {
        width: 100%;
    }

    .ExternalClass {
        width: 100%;
    }

    .ExternalClass * {
        line-height: 100%
    }

    td {
        -webkit-text-size-adjust: none;
    }

    a[href^=tel] {
        color: inherit;
        text-decoration: none;
    }

    .mob-hide {
        display: none !important;
    }

    div, p, a, li, td {
        -webkit-text-size-adjust: none;
    }

    td {
        text-decoration: none !important;
    }

    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }

    @media screen and (max-width:480px) {
        .pd {
            padding-left: 20px !important;
            padding-right: 20px !important;
        }
    }

    h3 {
        margin-top: 0;
        margin-bottom: 1;
    }

    #table {
        border-collapse: collapse;
    }

    #th, #td {
        padding: 10px;
        text-align: left;
        border: 1px solid #efefef;
    }

    /*                  tr:nth-child(even) {
          background-color: #eee;
        }*/

    /*                  tr:nth-child(odd) {
          background-color: #fff;
        }*/
    </style>

    <body style="margin:10 !important; padding:0px 0 0 0px !important; background-color:#FFFFFF;">
        <table width="480" style="max-width: 600px; width: 100%; margin: 0 auto; text-align: center;" cellpadding="0"
            cellspacing="0" border="0">
            <tr>
                <td valign="top" align="center" style="padding-top: 10">
                    <table border="0" align="center" cellpadding="0" cellspacing="10" width="100%">
                        <tr>
                            <td class="leftColumnContent">
                                <img src="http://services.google.com/fh/files/emails/ux_infra_logo_long.png"
                                    class="columnImage" width="230" alt="logoImageUrl"
                                    style="-ms-interpolation-mode:bicubic; height:auto; outline:none; text-decoration:none"
                                    height="auto"> 
                            </td>
                        <!--  removed  <td
                                style="font-family:Google Sans,Arial,sans-serif;font-size:16px;font-weight:700;color:#9aa0a6;letter-spacing:-0.31px;text-align:right">
                                UXI Automation</td>  -->
                        </tr>
                    </table>
                    <!-- Main card start -->
            <tr>
                <td align="center" valign="top" bgcolor="#ffffff"
                    style="-webkit-border-radius:0px; -moz-border-radius:0px; border-radius: 0px; border-left: 1px solid #efefef; border-top: 1px solid #efefef; border-right: 1px solid #efefef; border-bottom: 1px solid #efefef;">

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>         
                            <tr>
                                <td valign="top" align="left"
                                    style="font-family:Roboto, Helvetica, Arial sans-serif; font-size: 14px; line-height:24px; color: #414347; padding:30px 40px 30px 40px;">

                                    ${intro}
                                    </a>
                                    <br>
                                    <p>${body}</p>
                                    <p>${signature}</p>
    
                            <!-- Sign off ends -->
                        </tbody>
                    </table>
                </td>
            </tr>
            <!-- Main card end -->

    </body>

    </html>
      `
}