function moveToTransitQueue() {
  const formId = "1jcK76vqTrPpKva3zOtbkINhNxZlawJpsYWMNTG4fbvA"
  lockFNWrapper(moveToTransitQueue, formId)//(formId)

  function moveToTransitQueue(id) {
    // id = id || "1jcK76vqTrPpKva3zOtbkINhNxZlawJpsYWMNTG4fbvA"
    const spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    /** validate users response, and reason */
    isValidSelection(spreadsheet);
    formSubmitByActiveRange({ id, spreadsheet, cbArr: [clearContentRange] });

    function clearContentRange({ rowAsObject, status }) {
      let range = rowAsObject.getRangeOfRow();
      range.clearContent()
    }
    function setBackgroundColor({ rowAsObject, status }) {
      let setBackgroundColor = (color) => rowAsObject.getRangeOfRow().setBackgroundColor(color)
      status ? setBackgroundColor("#d9ead3") : setBackgroundColor("#fce5cd")
    };
    function isValidSelection(spreadsheet) {
      const aboveFirstRow = (activeRange) => activeRange.getRow() > 1;
      aboveFirstRow.msg = "Selection must be above first row"
      const singleActiveRange = (activeRange) => {
        let endRow = activeRange.getEndRow()
        let rowIdx = activeRange.getRow()
        if (rowIdx === endRow) {
          return true
        } else {
          throw ("Only works with single row selection")
        }
        return false
      }
      singleActiveRange.msg = 'Only valid for single selection'
      const confirmSelection = (activeRange) => {
        let sheet = activeRange.getSheet();
        let dataRange = sheet.getDataRange()
        let values = dataRange.getValues();
        let rowIdx = activeRange.getRow()
        let [headers] = values
        let studyNameIdx = headers.indexOf("What's the name of the study?")
        if (studyNameIdx === -1) { return false }
        let studyName = sheet.getRange(rowIdx, studyNameIdx + 1).getValue()

        const response = Browser.msgBox(`Please confirm study name: ${studyName}, to be sent to Transfer Queue`, Browser.Buttons.YES_NO);
        return response === 'yes' ? true : false
      }
      confirmSelection.msg = "Exited out of sending to Transfer Queue per client request"
      let inValidSelection = [aboveFirstRow, singleActiveRange, confirmSelection].find(fn => !fn(spreadsheet.getActiveRange()));
      return !inValidSelection ? true : ((msg) => { throw (msg) })(inValidSelection.msg)
      //[aboveFirstRow, singleActiveRange, confirmSelection].every( fn => fn(spreadsheet.getActiveRange())) ? true : (() => { throw('Invalid Selection')} )()



    }
  }

  function formSubmitByActiveRange({ id, spreadsheet, cbArr }) {
    /** get information about transfer */
    const reason = isValidReason();

    let { itemsList, formResponse, rowAsObject } = fetchQuestionAnswerFromTrix({ activeRange: spreadsheet.getActiveRange(), id });
    rowAsObject = addTranferData(rowAsObject)
    let isFormSubmit = setQuestionAnswerFromTrix({ itemsList, formResponse, rowAsObject });
    cbArr.forEach(cb => cb({ rowAsObject, status: isFormSubmit, id }))

    function addTranferData(props) {
      const getLdap = (str) => str.slice(0, str.indexOf('@'))
      const sessionEmail = Session.getActiveUser().getEmail();
      const sessionLdap = getLdap(sessionEmail)
      const ssName = spreadsheet.getName();

      
      return Object.assign(props, { "by ldap": sessionLdap, "from spreadsheet name": ssName, "reason for transfer": reason, "spreadsheet id": spreadsheet.getId() });


    }
    /*
     * Fetches reason response, and throws error if conditions not meet
     * @return str || throws error
     */
    function isValidReason() {
      const reason = Browser.inputBox(`Please let us know why you think this study was misrouted, including both the requestor’s ldap and PA.`);

      const checkResponses = (...args) => args.forEach(fn => fn(reason) && ((msg) => { throw (msg) })(fn.msg))

      const isCancelResponse = (str) => str === 'cancel';
      isCancelResponse.msg = "Your request for transfer will be cancelled. Please resubmit if you change your mind."

      const isEmptyResponse = (str) => str.trim() === ""
      isEmptyResponse.msg = "You must provide a reason for your request. If you are not sure, please cancel and resubmit later."

      checkResponses(isCancelResponse, isEmptyResponse)

      return reason
    }

    function setQuestionAnswerFromTrix({ itemsList, formResponse, rowAsObject }) {
      itemsList.forEach(populateForm);
      try {
        formResponse.submit();
        return true
      } catch (e) {
        thow(e)
        return false
      }

      function populateForm({ title, setFormResponse }) {
        let answer = rowAsObject.hasOwnProperty(title) ? rowAsObject[title] : null;
      
        if (!!answer) {
          setFormResponse(answer);
          Logger.log(`${title} populated with ${answer}`);
          return true
        } else {
          return false
        }
      }
    }
    function fetchQuestionAnswerFromTrix({ activeRange, id }) {
      /** validation */
      // single row
      let rowAsObject = fetchActiveRangeAsObj(activeRange);
      // id = id || "1jcK76vqTrPpKva3zOtbkINhNxZlawJpsYWMNTG4fbvA"
      // let {itemsList, formResponse} = ;
      try {
        let transifForm = getTransitForm(id)
        return Object.assign({}, { rowAsObject, ...transifForm })
      } catch (e) {
        throw (`Permission required to access transfer Form ${id} \n error:${e}`)
      }
    }
    function fetchActiveRangeAsObj(activeRange) {
      activeRange = activeRange || SpreadsheetApp.getActiveRange();
      let activeRow = activeRange.getRow();
      let sheet = activeRange.getSheet()
      let dataRange = sheet.getDataRange();
      let values = dataRange.getValues()
      let [header] = values;
      let row = values[activeRow - 1];
      let rowAsObj = convertToObj(header, row);
      let test = rowAsObj["Email"]

      Object.defineProperties(rowAsObj, {
        getRangeOfRow: {
          value: () => sheet.getRange(activeRow, 1, 1, sheet.getLastColumn()), // return range of column 4
          writable: false,
          enumerable: false,
        },
      })
      Object.defineProperties(rowAsObj, {
        getRangeByHeader: {
          value: (str) => {
            let headerIdx = header.indexOf(str)
            let idx = headerIdx !== -1 ? headerIdx + 1 : null
            let range = sheet.getRange(activeRow, idx)
            return range
          }, // return range of column 4
          writable: false,
          enumerable: false,
        },
      })
      return rowAsObj

      function convertToObj(headers, arr) {
        return headers.reduce((acc, next, idx) => {
          let cellAsObject = { [next]: arr[idx] }
          acc.hasOwnProperty(next) ? acc : Object.assign(acc, cellAsObject)
          return acc
        }, {})
      }
    }
    function getTransitForm(id) {
      id = id || "1jcK76vqTrPpKva3zOtbkINhNxZlawJpsYWMNTG4fbvA"
      const form = FormApp.openById(id)//('id')
      const formResponse = form.createResponse();
      const formItems = form.getItems();
      return {
        itemsList: getItems(formItems),
        formResponse
      }
      // let anon = questions.map(getItems)

      function getItems(formItems) {
        return formItems.flatMap(item => {
          let title = item.getTitle();

          
          let itemType = item.getType().name();
          let question = item

          return {
            title, question, itemType, item,
            setFormResponse: setFormResponse({ question, itemType, formResponse })
          }

        })
      }

      function setFormResponse({ question, itemType, formResponse }) {
        return function (answer) {
          let itemOptions = {
            'TEXT': (answer) => formResponse.withItemResponse(question.asTextItem().createResponse(answer)),
            'PARAGRAPH_TEXT': (answer) => formResponse.withItemResponse(questions[n].asParagraphTextItem().createResponse(answer)),
            'DATE': (answer) => formResponse.withItemResponse(question.asDateItem().createResponse(answer)),
            'LIST': (answer) => formResponse.withItemResponse(question.asTextItem().createResponse(answer))
          }
          let _setItemResponse = itemOptions.hasOwnProperty(itemType) ? itemOptions[itemType] : null
          try {
            _setItemResponse(answer);
            return true;
          } catch (e) {
            throw (e);
            return false
          }
        }
      }
    }
  }
}
function lockFNWrapper(fn, args) {
  const lock = LockService.getScriptLock()
  lock.waitLock(3000);
  try {
    fn(args)
  } catch (e) {
    lock.releaseLock()
    throw (e)
  }
  lock.releaseLock()
}