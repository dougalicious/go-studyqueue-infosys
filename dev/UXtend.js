
/**
 * Description: a copy of this version lives in the Library for UXtends
 * lib: https://script.google.com/corp/home/projects/1dlt3RWSKBYTHbrLfKWacbgFfjyIIG33BVAQz7u_oLv3fXfn-iUmPIIkv/edit
 * and is also referrenced @ https://script.google.com/corp/home/projects/10h0wNovWWn9gsidjTGAR3O5h8TnPTj6bLXqP62Uu1sr78HGKRURcxHFw/edit
 * the UXtend Tracker template
 * 
 * the code on the tracker template references the Library. i had reluctance to place this here as it requires importing the library: fn: Lib.getPodLead &&  fn: dailyActiveStudyPull
 * The event handler triggered when the selection changes in the spreadsheet.
 * @param {Event} e The onSelectionChange event.
 **/

function onActiveStudyIdEdit(e) {
  const STATIC = {
    //Spreadsheet external
    SS: {
      StaffingTrix: {
        id: "1ZoGLcVi_s7rZQgUx-Gan8rwK89yTTQsbuJm1BqTF1Vc",
        Personnel: "Personnel",
        COLHEAD: {
          PODLEAD: "POD LEAD",
          ldap: "ldap"

        }
      },
      UXTendTracker: {
        Id: "1Vkxq0jm_R4Py09BfujFoKC1EWtQJaJqDD30bJl4bqg4",
        SheetNames: {
          UnallocatedStudies: {
            Name: "UXtend Unallocated Studies",
            headerIdx: 3,
            bodyIdx: 4
          }
        },
      },
      GOSTUDYQUEUE: {
        ACTIVESHEETNAME: "Active Studies"
      }


    }
  }
  const activeSheetName = STATIC.SS.GOSTUDYQUEUE.ACTIVESHEETNAME;
  const compose = (...fns) => x => fns.reduceRight((y, f) => f(y), x);
  const getRangeBySheet = (sheet) => (rowIdx) => sheet.getRange(rowIdx, 1, 1, sheet.getLastColumn()).getValues();
  const isActiveSheet = ({ source }) => source.getActiveSheet().getName() === activeSheetName
  const isStudyIdColumn = ({ range }) => range.columnEnd === 1 && range.columnStart === 1
  const isNewValue = ({ oldValue }) => oldValue === undefined
  const isNotBlankNewValue = ({ newValue }) => newValue !== ""
  const isValidActiveStudyUpdate = (e) => {
    try {
      return [isActiveSheet, isStudyIdColumn, isNewValue, isNotBlankNewValue].every(fn => fn(e))
    } catch (e) {
      return false
    }
  }
  const isValidStudyType = ({ isRemoteModerated, isValidSupportType, ...props }) => !!isRemoteModerated && !!isValidSupportType
  try {
    if (isValidActiveStudyUpdate(e)) {
      Logger.log(`${JSON.stringify(e)}`)
      const rowAsProps = getRowAsProps(e);
      isValidStudyType(rowAsProps) && addUXtendTracker(rowAsProps)
    } else {
      Logger.log(`false`)
      return
    }
  } catch (e) {
    Logger.log(e)
  }


  function addUXtendTracker({ props, e, isRemoteModerated }) {
    Logger.log('addingUxtend data')
    const uxtendId = STATIC.SS.UXTendTracker.Id
    const unallocatedStudies = STATIC.SS.UXTendTracker.SheetNames.UnallocatedStudies.Name
    const unallocatedStudiesSheet = SpreadsheetApp.openById(uxtendId).getSheetByName(unallocatedStudies)
    const getRangeByRow = getRangeBySheet(unallocatedStudiesSheet)
    const [headerRow] = getRangeByRow(STATIC.SS.UXTendTracker.SheetNames.UnallocatedStudies.headerIdx);
    const isHeaderStartDate = (header) => "What is the targeted first date of the study?" === header
    const formattedDate = (date) => Utilities.formatDate(date, SpreadsheetApp.getActiveSpreadsheet().getSpreadsheetTimeZone(), "MM/dd/yyyy");
    const values = headerRow.reduce((acc, header, idx) => {
      let currentCell = isHeaderStartDate(header) ? formattedDate(props[header]) : header === "timestamp for moved to monitoring" ? new Date() : props.hasOwnProperty(header) ? props[header] : ""
      return currentCell !== null ? acc.concat(currentCell) : acc
    }, [])
    let podLeads
    let podLeadIdx = headerRow.indexOf("PL")
    try {
      podLeads = getPodLead(props["Pod"])
    } catch (e) {
      podLeads = `error: ${e}`
    }
    values[podLeadIdx] = podLeads
    unallocatedStudiesSheet.appendRow(values);

  }
  function getPodLead(podName) {
    const podNum = podName.replace(/pod\s*/gi, "")
    const sheet = SpreadsheetApp.openById(STATIC.SS.StaffingTrix.id).getSheetByName(STATIC.SS.StaffingTrix.Personnel)
    const { body } = getDataBySheet(sheet);
    const selectPodLeads = body.filter(props => props[STATIC.SS.StaffingTrix.COLHEAD.PODLEAD].includes(podNum)).filter(filterActive)
    const ldap = selectPodLeads.map(props => props[STATIC.SS.StaffingTrix.COLHEAD.ldap])
    return ldap.join(', ')

    function filterActive(props){
      let status = props.hasOwnProperty("In/Active") ? props["In/Active"] : "Not found"
      let isActive = /^active/gi.test(status)
      return isActive
    }
  }
  function getRowAsProps(e) {
    const spreadsheet = e.source
    const sheet = spreadsheet.getActiveSheet()
    const getRangeByRow = getRangeBySheet(sheet)
    const [headerRow] = getRangeByRow(1);
    const [currentRow] = getRangeByRow(spreadsheet.getActiveRange().getRow());
    const props = headerRow.reduce((acc, next, idx) => {
      let currentCell = currentRow[idx]
      acc[next] = currentCell
      return acc
    }, {})

    const regexIsValid = (regex, str) => regex.test(str)
    const getPropByQuestion = (str) => props[str]
    const studyType = props["Which type of study are you planning to do?"]

    const isRemoteModerated = regexIsValid(/Remote - moderated/gi, getPropByQuestion("Which type of study are you planning to do?"));
    const isValidSupportType = regexIsValid(/^UXI ResearchOps: Full recruitment support/gi, getPropByQuestion("What type of support do you need?"));
    // const isValidParticipantType =  regexIsValid(/external/gi, getPropByQuestion("What type of participants are you planning to use?"));
    Logger.log(`isRemoteModerated : ${isRemoteModerated}`)
    Logger.log(`isValidSupportType : ${isValidSupportType}`)
    return Object.assign(e, { isRemoteModerated, isValidSupportType, props })
  }

}
