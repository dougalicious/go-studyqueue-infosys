function populateOrangeColumnByRow(row){
  return isAnyBlank(orangeColumsValues(row)) ? populateEmptyOrangesColumnsCells().populateOrangeColsByRow(row) : row;

  function orangeColumsValues(row){
    return row.slice(27, 34);
  }
  function isAnyBlank(row){
    return row.filter( function(cell) { return cell === "";} ).length !== 0;
  }
}

function populateEmptyOrangesColumnsCells(){
  //var closedStudies2019Sheet = getValuesAndFormulasFromSheet("Closed Studies 2019")
  
  var whoWeCoverSheet = getValuesAndFormulasFromSheet("Who we cover");
  var blankHeadersSearch = ["Researcher Location",
                            "Cost Center Code (recruiters do not edit this column)",
                            "Cost Center Name (recruiters do not edit this column)",
                            "Product Area (recruiters do not edit this column)",
                            "Are they on the list of supported cost centers?", 
                            "Pod",
                            "BU Code"
                           ];
  return populateStudies(blankHeadersSearch,whoWeCoverSheet);
 
  
  // execute below as iife to run column population on the two sheets listed below
  // sample below of how sheets data are passed in
//  (function (){
//    var activeStudiesSheet = getValuesAndFormulasFromSheet("Active Studies")
//    var closedStudies2019Sheet = getValuesAndFormulasFromSheet("Closed Studies 2019")
//    updateStudiesSheetOrangeColumns.populateOrangeColsBySheet(closedStudies2019Sheet, "Closed Studies 2019");
//    updateStudiesSheetOrangeColumns.populateOrangeColsBySheet(activeStudiesSheet, "Active Studies" )
//  })
  
  
  /*
  * Takes base layer data: headers tobe filter from 
  * @params Object( Array[Headers], getValuesAndFormulasFromSheet("whoWeCoverSheet")
  * @return fn sheet data from fn(getValuesAndFormulasFromSheet)
  */
  function populateStudies(blankHeadersSearch,whoWeCoverSheet){
    return {
      populateOrangeColsBySheet,
      populateOrangeColsByRow 
    }
      
      /* executes populate fields on sheet data passed in
       * @param( Object(getValuesAndFormulasFromSheet(SheetName)), String(SheetName))
       * @return undefined
       */
      function populateOrangeColsBySheet(processingSheet, activeSheetName){
        var sheetByNameofActiveSS = getSheetByNameofActiveSS(activeSheetName) || getSheetByNameOfActive('Active Studies')
        var rowsToBePopulated = processingSheet.filter(filterByBlankFromHeader(blankHeadersSearch))
        var testings = rowsToBePopulated.every(testValueToRangeEquals)
        rowsToBePopulated.forEach( populateByRow )
      }
      function populateOrangeColsByRow(rowTobeUpdated){
        var emailAddress, ldap;
        var getCorrespondingHeader = columnMapToWhoWeCover(blankHeadersSearch)
        var whoWeCoverLdap = whoWeCoverSheet.filter(function( byRow ){
          return byRow.rowAsObject["Ldap"] === ldap;
        })[0]; 
        var isUpdateBySheet = (rowTobeUpdated.hasOwnProperty('rowAsObject') || rowTobeUpdated.__proto__.hasOwnProperty('rowAsObject'))
        var isUpdateByRow = !isUpdateBySheet
        if ( isUpdateByRow ) {
          var o = getHeaderAsObjectFromSheet(SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Active Studies"))(rowTobeUpdated)
          emailAddress = o.rowAsObj["Email Address"];     
          ldap = emailAddress.substr(0, emailAddress.indexOf("@google.com"));
          whoWeCoverLdap = whoWeCoverSheet.filter(function( byRow ){
            return byRow.rowAsObject["Ldap"] === ldap;
          })[0]; 
          var rowAsObject = Object.create(o.headerObj)
          for ( let header in rowAsObject){
           rowAsObject[header] = rowTobeUpdated[rowAsObject[header]] || ""
          }      
         if (whoWeCoverLdap === undefined) {
           //no matching ldap found
           return rowTobeUpdated; 
         }
      
         var firstArg = Object.keys(whoWeCoverLdap.rowAsObject)
         var bh = blankHeadersSearch
         //  debugger
         blankHeadersSearch.forEach( function (blankHeader) {
           let whoWeCoverHeader = getCorrespondingHeader(Object.keys(whoWeCoverLdap.rowAsObject), blankHeader)
           let currRow = rowAsObject.hasOwnProperty(blankHeader)
           let val = rowAsObject[blankHeader]
           let whoWeCoverHeaderValue = whoWeCoverLdap.rowAsObject[whoWeCoverHeader]
           if ( val === "" ) {
             // updates empty value of column with ldap match from "Who We Cover" sheet
             
             rowAsObject[blankHeader] = whoWeCoverHeaderValue
           }
         })
         // convert obj back to arr
      
         var currArrLen = rowTobeUpdated.length
         var next = Object.keys(rowAsObject).reduce(function(acc,next,idx){
           return acc.concat(rowAsObject[next]);
         },[]);
      
         var nextArrLen = next.length
         var sameLen = currArrLen === nextArrLen;
         return sameLen ? next : rowTobeUpdated
        } else {
         emailAddress = rowTobeUpdated.rowAsObject["Email Address"];
         ldap = emailAddress.substr(0, emailAddress.indexOf("@google.com"));
         updateCell( whoWeCoverLdap, blankHeadersSearch)
    }
    function updateCell( whoWeCoverLdap, blankHeadersSearch){
      blankHeadersSearch.forEach( function(blankHeader) {
        var isValidLdap = !!whoWeCoverLdap
        var correspondingHeader = isValidLdap && getCorrespondingHeader(Object.keys(whoWeCoverLdap.rowAsObject), blankHeader)
        var currValue = rowTobeUpdated.rowAsObject[blankHeader] 
        var nextValue = isValidLdap ? whoWeCoverLdap.rowAsObject[correspondingHeader] : false
        var rowIdx = rowTobeUpdated.rowIdx
        var colIdx = rowTobeUpdated.headerObj[blankHeader] + 1
        var range = sheetByNameofActiveSS.getRange(rowIdx,colIdx)
        var value = range.getValue()
        var bgColorCell = range.getBackground()
        if (!isValidLdap && value === "") { // not ldap user and currValue blank
          // no updates to be made for Ldap account's not found in "Who we cover"
          //range.setFontColor("PINK")
          range.setBackground('#DCDCDC')
        } else if (isCurrCellDiffWhoWeCoverSheet()) { // 
          // keep exisiting value in place
          // cell values differ existing compared to "Who we cover"
          // range.setBackground('#D3D3D3')
        } else if (doPopulateBlankCell()){
          // replaceBlankCell with corresponding Ldap "Who we cover" data
          range.setBackground('#f5f5dc')
          range.setValue(nextValue)
        } else {
          // no else
        }
        function doPopulateBlankCell() { return ((currValue != nextValue && value === "") || range.getColor === "#f5f5dc" || value === 'undefined')}
        function isCurrCellDiffWhoWeCoverSheet() {return ( currValue !== "" && nextValue != currValue && isValidLdap)}
      })
      
    }
  }
}

function columnMapToWhoWeCover(headersList){
  var options = {
    "Product Area (recruiters do not edit this column)" : "Business Unit",
    "Are they on the list of supported cost centers?": "Support level",
    "Researcher Location": "Location",
    "Cost Center Code (recruiters do not edit this column)": "Cost Center Code",
    "Cost Center Name (recruiters do not edit this column)": "Cost Center Name",
    "BU Code": "BU Code",
    "Pod": "pod"
  }
  
  var headerMatch = Object.keys(options).length === headersList.length
  if (!headerMatch) { throw ("Orange Header Values have changed, and can no longer be mapped"); }
  
  return function getCorrespondingHeader(keys, currentHeader ){
    var isFound = options.hasOwnProperty(currentHeader)
    if (isFound) { return options[currentHeader];}
    var regExpListBestFit = keys.filter(function(key){ 
      var regex = new RegExp(key,'g')
      return currentHeader.match(regex)
      Logger.log('ran best fit');
    });
    return regExpListBestFit.length === 1 ? regExpListBestFit[0] : 
    regExpListBestFit.length === 2 ? regExpListBestFit[1] : (function() {debugger ; throw('unable to find matching header in "Who We Cover Sheet" for ' +currentHeader)})()
  }
}



function filterByBlankFromHeader(headers){
  return function(byRow){
    var rowAsObject = byRow.rowAsObject;
    return headers.filter( function (header) {
      return (rowAsObject[header] === "" || rowAsObject[header] === "undefined")
    }).length !== 0 ? true : false;
  }
}


function getSheetByNameofActiveSS(sheetName){
  return SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheetName)
}

function getValuesAndFormulasFromSheet(sheetName, startIdx = 1){
  var sheet = getSheetByNameofActiveSS(sheetName)
  if (!sheet){
    debugger
    throw ("unable to find sheet name: " +sheetName+ "within url: " + sheet.getUrl());
  }
  var dataRange = sheet.getDataRange();
  var values = dataRange.getValues();
  var numRows = values.length -1
  var getHeaderObjByRow = getHeaderAsObjectFromSheet(sheet, startIdx);
  
  var data = [];
  
  for ( var i = startIdx ; i < numRows; i++){
    var row = values[i]
    var o = getHeaderObjByRow(row)
    data.push(
      Object.create({
        values: values[i],
        range: sheet.getRange(i+1, 1, 1, values[i+1].length),
        rowIdx: i+1,
        rowAsObject: o.rowAsObj,
        headerObj: o.headerObj
      })
    )
  }
  return data 
}

function getHeaderAsObjectFromSheet(sheet, headerIdx = 1){
  var dataRange = sheet.getDataRange()
  var dataRangeValues = dataRange.getValues();
  var headers = pullRowByIdx(dataRangeValues)(headerIdx)
  var headerObj = headers.reduce(function(acc, next, idx) {
    if (acc.hasOwnProperty(next)){
      throw( "Error Duplicate Header Name found")
    }
    if(next === "") { return acc; }
    acc[next] = idx;
    return acc
  },{});
  
  return function(values){
    var rowAsObj = Object.create(headerObj)
    for ( var header in headerObj){
      var colIdx = headerObj[header]
      rowAsObj[header] = values[colIdx]
    }
    return {
      rowAsObj,
      headerObj
    };
    }
      
      function pullRowByIdx(arr){
      return function(idx){
      // minus one accounts for range values have base of 1 instead of zero like arrays
      return  Array.isArray(arr) ? arr.length > 0 ? arr[idx-1] : arr : null
    }
  }
}



function testValueToRangeEquals(rowAsObj){
  var values = rowAsObj.values
  var range = rowAsObj.range
  var testRangeRow1 = range.getValues()[0]
  var isEqual = JSON.stringify(values) === JSON.stringify(testRangeRow1);
  return isEqual;
}

}
//wrapping all a function so no namespace overlap