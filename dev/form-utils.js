/* prefilled
https://docs.google.com/forms/d/e/1FAIpQLSeuTUhjUuYa3_L5YBnwrUN0ZU0ecItGoSuwW6suCoGKOE2t9g/viewform?usp=pp_url
&entry.767559480=email+address
&entry.629171496=What+type+of+support+do+you+need?
&entry.1262557874=Where+in+the+world+would+the+participants+in+your+study+be+located?
&entry.582087023=How+many+participants+do+you+need+for+this+study?
&entry.1770732082=What%27s+the+name+of+the+study?
&entry.1973243395=We%27ve+captured+your+username+already.+Any+other+researchers+working+with+you+on+this+study?
&entry.585203819=2021-07-14
&entry.10426583=Which+type+of+study+are+you+planning+to+do?
&entry.179855146=If+we+are+unable+to+find+the+participants+you%27re+looking+for,+would+you+prefer+to:
&entry.1564712347=Do+your+participants+belong+to+any+of+the+following+categories?
&entry.2015381600=What+type+of+participants+are+you+planning+to+use?
&entry.310333343=If+you+are+planning+to+use+TVCs+or+minors,+who+is+the+pCounsel+who+has+approved+this+study?
&entry.496682594=In+which+city/region/country+will+study+sessions+take+place?
&entry.1928894410=If+you+or+another+researcher+received+recruiting+support+for+a+previous+study+with+similar+criteria,+please+let+us+know+the+recruiter,+study+name,+and+approximate+dates.
&entry.1962578154=Paste+the+link+to+your+Study+Spreadsheet+here.+Ensure+that+your+Screener+Form+has+been+linked+to+it+and+that+the+sharing+settings+for+both+are:+private,+with+uxr-operations-share@google.com+added+as+an+editor/collaborator.
&entry.79971332=Please+include+a+link+to+your+research+plan+or+any+other+relevant+documentation+that+would+be+helpful+for+this+recruitment+effort.
&entry.949791123=Anything+else+we+should+know?+If+you+will+be+out+of+office+or+on+vacation+before+your+study+begins,+please+indicate+the+dates+and+the+primary+contact+person+while+you%27re+gone.
&entry.573578680=Researcher+Location&entry.1226996253=Cost+Center+Code+(recruiters+do+not+edit+this+column)
&entry.1116327745=Cost+Center+Name+(recruiters+do+not+edit+this+column)
&entry.42177690=Product+Area+(recruiters+do+not+edit+this+column)&entry.1744110257=Are+they+on+the+list+of+supported+cost+centers?
&entry.569051020=Pod&entry.866014022=BU+Code&entry.1868685964=Manager+List
&entry.1055388370=unified_rollup_level_1&entry.1254633390=by+ldap
&entry.1011510560=from+spreadsheet+name
&entry.1438556845=spreadsheet+id&entry.602316223=reason+for+transfer
*/

function fetchApprovalFromQueryToHeaderMapping(props) {
  const spreadsheetTmz = SpreadsheetApp.getActiveSpreadsheet().getSpreadsheetTimeZone()
  const formatDate = (date) => Utilities.formatDate(date, spreadsheetTmz, 'YYYY-MM-dd')
  let entries = {
    'entry.1804961743': props.emailAddress,
    'entry.215893547': props.supportType, //=What+type+of+support+do+you+need?
    'entry.795101687': props.studyLocation, //=Where+in+the+world+would+the+participants+in+your+study+be+located?
    'entry.632151648': props.participantsNum, //=How+many+participants+do+you+need+for+this+study?
    'entry.625442296': props.studyName, //What's+the+name+of+the+study?
    'entry.44957032': props.otherResearcher, //We've+captured+your+username+already.+Any+other+researchers+working+with+you+on+this+study?
    'entry.322618690': formatDate(props.targetDate), //2021-07-13
    'entry.1539067757': props.typeOfStudy, //Which+type+of+study+are+you+planning+to+do?
    'entry.545388425': props.participantPreference, //If+we+are+unable+to+find+the+participants+you're+looking+for,+would+you+prefer+to:
    'entry.2112183337': props.participantCategory, //Do+your+participants+belong+to+any+of+the+following+categories?
    'entry.1080127653': props.participantType, //What+type+of+participants+are+you+planning+to+use?
    'entry.773706764': props.pcounsel, //If+you+are+planning+to+use+TVCs+or+minors,+who+is+the+pCounsel+who+has+approved+this+study?
    'entry.31662527': props.cityRegionCountryLocation, //In+which+city/region/country+will+study+sessions+take+place?
    'entry.1666382629': props.similarStudy, //If+you+or+another+researcher+received+recruiting+support+for+a+previous+study+with+similar+criteria,+please+let+us+know+the+recruiter,+study+name,+and+approximate+dates.
    'entry.1867002993': props.studySpreadsheetLink, //Paste+the+link+to+your+Study+Spreadsheet+here.+Ensure+that+your+Screener+Form+has+been+linked+to+it+and+that+the+sharing+settings+for+both+are:+private,+with+uxr-operations-share@google.com+added+as+an+editor/collaborator.
    'entry.2017910400': props.researchPlan, //Please+include+a+link+to+your+research+plan+or+any+other+relevant+documentation+that+would+be+helpful+for+this+recruitment+effort.
    'entry.1560391638': props.otherData, //Anything+else+we+should+know?+If+you+will+be+out+of+office+or+on+vacation+before+your+study+begins,+please+indicate+the+dates+and+the+primary+contact+person+while+you're+gone.
    'entry.66166391': props.researcherLocation, //Researcher+Location
    'entry.57935563': props.costCenterCode, //Cost+Center+Code+(recruiters+do+not+edit+this+column)
    'entry.329689144': props.costCenterName, //Cost+Center+Name+(recruiters+do+not+edit+this+column)
    'entry.361059011': props.productArea, //Product+Area+(recruiters+do+not+edit+this+column)
    'entry.79469294': props.supported, //Are+they+on+the+list+of+supported+cost+centers?
    'entry.1193152887': props.pod, //Pod
    'entry.1703610094': props.buCode, //BU+Code
    'entry.1769140199': props.mangerList, //Manager+List'
    'entry.1414735975': props.unifiedRollup, //unified_rollup_level_1
    'entry.525115626': props.fromLdap, //by+ldap
    'entry.1859611422': props.fromSpreadsheetName, //from+spreadsheet+name
    'entry.1998626020': props.transferReason, //reason+for+transfer
    // 'entry.1688176278': approvalStatus, //Approve || Deny
  }


  return entries
}
