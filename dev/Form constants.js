const FORM = {
  QUESTION: {
    StudySSLink: "Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uer-recruiters@google.com added as an editor/collaborator."
  }
}

