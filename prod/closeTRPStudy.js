function closeTRPStudy() {
  // removed "close this trp study"
  return 
  closeStudy(processTRPStudy)
}



function processTRPStudy(rowIdx, row) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getActiveSheet();
  var getColIdxByHeader = getHeaderObjBySheet(sheet);
  var rowIdx = rowIdx || sheet.getActiveRange().getRow();
  const currentTRPList = getEmailsOfTRPMomaBadge();


  const sendTrpSurveyFn = compose(sendTRPSurvey, getTRPSurveyAsHTML)
  sendTrpSurveyFn(rowIdx, row)


  /*
    move study to closed study tab
    collect study name
    getSession user email.
    
  */
  function sendTRPSurvey(arr) {
    // if muliple emails send individually, otherwise single
    debugger
    arr.forEach(sendSingleTrpWelcomEmail)


    function sendSingleTrpWelcomEmail({ emailAddress, emailBody }) {
      try {
        debugger
        // GmailApp.sendEmail(emailAddress, "[TEST]We want to hear about your TRP Experience!", emailBody, { htmlBody: emailBody, noReply: true, });
        //dev test
        GmailApp.sendEmail(emailAddress, "We want to hear about your TRP Experience!", emailBody, { htmlBody: emailBody, noReply: true, });
        Logger.log('email sent to ' + emailAddress)
        return true
      } catch (error) {
        debugger
        const ssId = SpreadsheetApp.getActiveSpreadsheet().getId()
        GmailApp.sendEmail('uxi-automation@google.com', 'TRP emails failed', `ssId => sendSingelTrpWelcome to ${emailAddress} fn:sendSingleTrpWelcomEmail`, { noReply: true })
        Logger.log(error); return false
      }
    }
  }

  function getTRPSurveyAsHTML(rowIdx, row) {
    var currentRow = row;
    const valueByHeader = (header, regex) => regex === undefined ? currentRow[getColIdxByHeader(header)] : currentRow[String(getColIdxByHeader(header))].replace(regex, "+")
    var studyName = valueByHeader("What's the name of the study?", /\s+/g);
    var studyId = valueByHeader("Study ID", /\s+/g);
    var emailAddress = valueByHeader("Email Address");

    var otherResearchers = compose(getLdapsFromStr, valueByHeader)("We've captured your username already. Any other researchers working with you on this study?")

    var trpWelcomeEmailList = [emailAddress, ...otherResearchers] //.filter(emailAddress => !currentTRPList.includes(emailAddress))
    debugger
    const getEmailAndLdapFn = (formUrl) => (emailAddress) => { return { emailAddress, ldap: getLdap(emailAddress), formUrl } }
    const getFirstNameFn = (ldap) => {
      try {
        let person = getGooglerInfo(ldap);
        return person.hasOwnProperty('firstName') ? person.firstName : ldap
      } catch (e) {
        return ldap
      }
    }

    var formUrl = "https://docs.google.com/forms/d/e/1FAIpQLSdga8yAWc81218YYEQk3ZUc21ebr_G_dfHMFqK56z02UtnU0Q/viewform?entry.579004536=" + studyName + "&entry.1318008270=" + studyId
    var ldap = getLdap(emailAddress)
    Logger.log('"' + ldap + '"')
    const getEmailAndHtmlFn = ({ emailAddress, ldap, formUrl }) => { return { emailAddress, emailBody: getTRPHtml({ firstName: getFirstNameFn(ldap), formUrl }) } }
    const getEmailWithLdap = compose(getEmailAndHtmlFn, getEmailAndLdapFn(formUrl));

    return trpWelcomeEmailList.map(getEmailWithLdap);

    function getTRPHtml({ firstName, formUrl }) {
      // var doc = DocumentApp.openById("11FqDrNHKpp4bgsz3sa3GbqbsrMLZD0PfJ6EzFEAUquU")
      // .getBody().getText();
      var doc = getTRPASHTML()
      var emailBody = doc
        .replace("{{firstName}}", firstName)
        .replace(/{{formUrl}}/gi, formUrl);
      debugger
      return emailBody;
    }
    function getLdap(userName) {
      return (userName.indexOf('@') !== -1) ? userName.slice(0, userName.indexOf('@')) : userName;
    }
    function getLdapsFromStr(str) {
      // would be nice for to filter out n/a ||  !/^n\/a$/gi.test(str)
      return str.split(/[\s,]+/g)
        .filter(str => str !== "")
        .map((str) => {
          if (typeof str !== 'string') { return (() => { throw ('${next} is not a string') }) }
          if (/@google.com/gi.test(str)) {
            return str
          } else if (/@/g.test(str)) {
            if (str.indexOf('@') === 0) {
              return str.slice(1).concat('@google.com')
            } else {
              return str.concat('google.com')
            }
          } else {
            return str.concat('@google.com')
          }
        }, [])
    }

  }
  function getLdap(userName) {
    return (userName.indexOf('@') !== -1) ? userName.slice(0, userName.indexOf('@')) : userName;
  }
  function getEmailsOfTRPMomaBadge() {
    const ss = SpreadsheetApp.getActiveSpreadsheet()
    const sheet = ss.getSheetByName("TRPBadges")
    const { body } = getDataBySheet(sheet)
    return body.map(({ "Recipient": recipient }) => recipient)
  }
}





































//@return String: html of trp close study reply
function getTRPASHTML() {
  return `
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta charset="utf-8">
      <title>Bulleted Email Starter Template</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!--[if !mso]><!-->
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <!--<![endif]-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta name="robots" content="no index">
      <!--[if mso | ie]>
      <style>
         .sup {
         vertical-align: 1px !important; 
         font-family:Roboto, Arial, Helvetica, sans-serif; font-size: 100% !important;
         }
         .pd2{
         padding-top:0px !important;
         }
      </style>
      <![endif]-->
      <!--[if ie]>
      <style>
         .sup {
         vertical-align: 6px !important; 
         font-family:Roboto, Arial, Helvetica, sans-serif; font-size: 80% !important;
         }
      </style>
      <![endif]-->
      <style>
         body{
         font-family:Roboto, Arial, Helvetica, sans-serif;
         font-size:15px; 
         color:#000000;
         -webkit-text-size-adjust: none !important;
         -webkit-font-smoothing: antialiased !important; 
         -ms-text-size-adjust: none !important;
         }
         table, tr, td { 
         mso-table-lspace:0pt; 
         mso-table-rspace:0pt; 
         }
         a:link, a:visited, a:hover, a:active {
         color: #4285f4;
         text-decoration: none;
         }
         .appleLinks a {color:#000000 !important; text-decoration:none !important;}
         strong {
         font-weight: bold !important;
         }
         em {
         font-style: italic !important;
         }
         .yshortcuts a span {
         color: inherit !important;
         border-bottom: none !important;
         }
         html {
         -webkit-text-size-adjust: none;
         -ms-text-size-adjust: 100%;
         }
         .ReadMsgbody1 {
         width: 100%;
         }
         .ExternalClass {
         width: 100%;
         }
         .ExternalClass * {
         line-height: 100%
         }
         td {
         -webkit-text-size-adjust: none;
         }
         a[href^=tel] {
         color: inherit;
         text-decoration: none;
         }
         .mob-hide {
         display:none !important;  }
         div, p, a, li, td { -webkit-text-size-adjust:none; }
         td { text-decoration:none !important; }
         a[x-apple-data-detectors] {
         color: inherit !important;
         text-decoration: none !important;
         font-size: inherit !important;
         font-family: inherit !important;
         font-weight: inherit !important;
         line-height: inherit !important;
         }  
         @media screen and (max-width:480px) {
         .pd{
         padding-left: 20px !important;
         padding-right: 20px !important;
         }
         } 
      </style>
   </head>
   <body style="margin:10 !important; padding:0px 0 0 0px !important; background-color:#FFFFFF;">
      <!-- Preview text starts -->
      <p style=" padding-top:0; font-size:0px; line-height:0px; color:#f2f2f2; background-color:#f2f2f2;" align="center">Tell us your experience with study support</p>
      <div style="display: none; max-height: 0px; overflow: hidden;">
         &nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
      </div>
      <!-- Preview text ends -->    
      <!-- Main table width 480px starts -->
      <!--[if (gte mso 9)|(IE)]>
      <table align="center" width="480" cellspacing="0" cellpadding="0">
      <tr>
         <td>
            <![endif]-->
            <table width="480" style="max-width: 600px; width: 100%; margin: 0 auto; text-align: center;" cellpadding="0" cellspacing="0" border="0">
               <tr>
                  <td valign="top" align="center" style="padding-top: 10">
                     <!-- 2 column header starts -->
                     <table width="100%" cellpadding="10" cellspacing="10" border="0" id="templateColumns">
                        <tr>
                           <td align="center" valign="top" width="50%" class="templateColumnContainer">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                 <tr>
                                    <td class="leftColumnContent">
                                       <img src="http://services.google.com/fh/files/emails/ux_infra_logo_long.png" width="240" alt="Google" style="-ms-interpolation-mode:bicubic; height:auto; outline:none; text-decoration:none" height="auto">
                                    </td>
                                 </tr>
                              </table>
                           </td>
                           <td align="center" valign="middle" width="50%" class="templateColumnContainer">
                              <table border="0" align="center" cellpadding="0" cellspacing="10" width="100%">
                              </table>
                           </td>
                        </tr>
                     </table>
                     <!-- 2 column header ends -->
                     <!-- Main card start -->
               <tr>
                  <td align="center" valign="top" bgcolor="#ffffff" style="-webkit-border-radius:0px; -moz-border-radius:0px; border-radius: 0px; border-left: 1px solid #efefef; border-top: 1px solid #efefef; border-right: 1px solid #efefef; border-bottom: 1px solid #efefef;">
                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                           <!-- Spacer starts -->  
                           <tr>
                              <td align="center" width="100%" style="display: table-cell; text-align: center; padding: 10px 0 10px 0;">&nbsp;</td>
                           </tr>
                           <!-- Spacer ends -->  
                           <!-- Title starts -->
                           <tr>
                              <td valign="top" align="center" style="font-family:Google Sans, Roboto, Helvetica, Arial sans-serif; font-size: 18px; font-weight: 500; line-height:36px; color: #202124; padding:0px 40px 0px 40px; letter-spacing: -0.31px">Trusted Research Pals Feedback</td>
                           </tr>
                           <!-- Title ends -->
                           <!-- Paragraph starts -->
                           <tr>
                              <td valign="top" align="left" style="font-family:Roboto, Helvetica, Arial sans-serif; font-size: 14px; line-height:24px; color: #414347; padding:20px 40px 20px 40px;">Hi {{firstName}},<br><br>
                                 We hope you had a dynamic study and gained impactful insights. Please take a few minutes to complete the <a href={{formUrl}} >Trusted Research Pals Feedback Form</a> as this will enable us to continue offering this service and improve it for future use. <br><br>
                           </tr>
                           <!-- Paragraph ends -->
                           <!-- Button starts -->
                           <tr>
                              <td align="center" valign="top" style="padding:0px 0px 0px 0px; padding-bottom: 50px;">
                                 <table align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                       <td>
                                       <table align="center" border="0" cellspacing="0" cellpadding="0">
										<tr>
										  <td align="center" style="border-radius: 3px;" bgcolor="#4285f4"><a href={{formUrl}} target="_blank" style="font-size: 16px; font-family: Google Sans, Roboto, Helvetica, Arial, sans-serif; font-weight: 500; letter-spacing: -0.31px; color: #ffffff; text-decoration: none; text-decoration: none;border-radius: 3px; padding: 11px 19px 11px 19px; border: 1px solid #4285f4; display: inline-block;"><!--[if gte mso 15]>&nbsp;&nbsp;&nbsp;<![endif]-->Take the survey<!--[if gte mso 15]>&nbsp;&nbsp;&nbsp;<![endif]--></a></td>
										</tr>
									  </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <!-- Button end -->
                           <!-- Sign off starts -->
                           <tr>
                              <td valign="top" align="left" style="font-family:Roboto, Helvetica, Arial sans-serif; font-size: 14px; line-height:24px; color: #414347; padding:0px 20px 40px 40px;">Thank you,<br> <br />
                                 <a href="mailto:uxi-trp@google.com">UXI Trusted Research Pals</a>  (uxi-trp@)
                               
                              </td>
                           </tr>
                           <!-- Sign off ends -->                             
                        </tbody>

                     </table>
                  </td>
               </tr>
               <!-- Main card end -->
               <!-- Blank space start -->
               <tr>
                  <td height="15" align="left" valign="top" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;mso-line-height-rule:exactly;line-height:15px;font-size: 1px; color: #ffffff">
                     <!--[if gte mso 15]>&nbsp;<![endif]-->
                  </td>
               </tr>
               <!-- Blank space end -->
               <!-- Blank space starts -->
               <tr>
                  <td height="15" align="left" valign="top" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;mso-line-height-rule:exactly;line-height:15px;">
                     <!--[if gte mso 15]>&nbsp;<![endif]-->
                  </td>
               </tr>
               <!-- Blank space ends -->
               <!-- Main ends -->
               <!--[if (gte mso 9)|(IE)]> 
               </td>
               </tr>
            </table>
            <![endif]-->
            <!-- Main table width 480px ends -->
   </body>
  `
}