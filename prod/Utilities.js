/* OWNER: dougecox, uxi-automation
 * SOURCES: go/studyqueue
 *
 * DESCRIPTION: use for sheet name transition. the script checks left to right searching for a valid sheet name when/if it finds it then returns the sheet
 * @date 07/17/2020 https://b.corp.google.com/issues/158816791 | to capture any edge cases while in transition
 * @param {(Object|Spreadsheet)} -description ie takes a number
 * @param {(Array| Strings})
 * @return {(Object:Sheet)} - description ie Date object 
 */

function getSingleSheetByNames(ss = SpreadsheetApp.getActiveSpreadsheet(), arr = ["Form Responses 2"]) {
  return ss.getSheetByName(arr.find(isValidSheet))

  function isValidSheet(name) {
    return ss.getSheetByName(name) === null ? false : true
  }
}

function promptConfirmationClaimStudy(msg, { ...args}){
  const notes = Object.entries(args).map( (arr) =>  `\\n${arr[0]}: ${arr[1]}`).join("");
  const response = Browser.msgBox(`${msg} ${notes}`, Browser.Buttons.YES_NO);
  return ( response === "yes" ? true : false) 
}

function getLdapsFromStr(str) {
  return str.split(/[\s,]+/g)
    .map((str) => {
      if (typeof str !== 'string') { return (() => { throw ('${next} is not a string') }) }
      if (/@google.com/gi.test(str)) {
        return str
      } else if (["NA", "N/A", ""].some(value => value === str)) {
        return null
      } else if (str.indexOf('@') === 0) {
        return str.slice(1).concat('@google.com')
      } else if (/@/g.test(str)) {
        return str.concat('google.com')
      } else {
        return str.concat('@google.com')
      }
    }, [])
    .filter(str => str !== null || str === "")
}

